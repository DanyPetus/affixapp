// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD5C8fX0HBduNfwuLlPSCEfkjnTG88r31U",
    authDomain: "affix-project.firebaseapp.com",
    databaseURL: "https://affix-project.firebaseio.com",
    projectId: "affix-project",
    storageBucket: "affix-project.appspot.com",
    messagingSenderId: "217322702078",
    appId: "1:217322702078:web:cc4213e1fa80d87bdf51af",
    measurementId: "G-28RBHLTJ7M"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
