import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-emergencies',
  templateUrl: './emergencies.page.html',
  styleUrls: ['./emergencies.page.scss'],
})
export class EmergenciesPage implements OnInit {
  machines = [];

  datos;
  // Seleccionamos o iniciamos el valor '0' del <select>
  opcionSeleccionado: string = '0';
  uidMachine: string = '';
  nameMachine: any;
  uidAdmin: any;

  date: any = '';
  hour: any = '';
  comentario: any = '';

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
    private alertCtrl: AlertController,
  ) {}

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getMachinesAssignedUsers(user.uid);
      }
    )
  }

  getMachinesAssignedUsers(uid) {
    this.firestoreService.getMachinesUser(uid)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const machine = doc.data();
          machine.uid = doc.id;
          this.machines.push(machine);
        });
      });
  }

  capturar() {
    // Pasamos el valor seleccionado a la variable verSeleccion
    this.uidMachine = this.opcionSeleccionado;
    console.log(this.uidMachine)
  }

  getDate() {
    console.log(this.date);
  }

  getHour() {
    console.log(this.hour);
  }

  saveEmergency() {
    this.getMachineByUid();
  }

  async getMachineByUid() {
    if (this.uidMachine === ''){
      this.alertMsj(
        "Datos incompletos",
        "Por favor, seleccione la máquina"
      );
    } else if (this.date === ''){
      this.alertMsj(
        "Datos incompletos",
        "Por favor, seleccione la fecha"
      );
    } else if (this.hour === ''){
      this.alertMsj(
        "Datos incompletos",
        "Por favor, seleccione la hora"
      );
    } else if (this.comentario === ''){
      this.alertMsj(
        "Datos incompletos",
        "Por favor, seleccione el comentario"
      );
    } else {
      await this.firestoreService.getMachineByUid(this.uidMachine).then(machine => {
        const machineInfo = machine.data();
        this.nameMachine = machineInfo.name;
        this.uidAdmin = machineInfo.createBy;
        console.log(this.nameMachine);
        console.log(this.uidAdmin);
        this.getUserUid();
      });
    }
  }

  getUserUid() {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.handleCreateEmergencyService(user.uid);
      }
    )
  }

  handleCreateEmergencyService(uidUser) {
    const serviceInfo = {
      uidAdmin: this.uidAdmin,
      uidMachine: this.uidMachine,
      machineName: this.nameMachine,
      uidUser: uidUser,
      date: new Date(this.date),
      hour: this.hour,
      commentary: this.comentario,
      status: 2,
      createBy: uidUser,
      createDate: new Date(),
      updateBy: uidUser,
      updateDate: new Date()
    };
    console.log(serviceInfo);

    this.firestoreService.createEmergencyService(serviceInfo).then(() => {
      this.alertMsj(
        "Servicio de Emergencia Guardado",
        "El servicio de emergencia fue creado con exito."
      );
    });
  }


  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      mode: 'ios',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }

}
