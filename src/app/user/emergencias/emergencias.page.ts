import {
  Component
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-emergencias',
  templateUrl: './emergencias.page.html',
  styleUrls: ['./emergencias.page.scss'],
})
export class EmergenciasPage {
  emergencyServices = [];
  emergencyServiceForm: FormGroup;

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
    private fb: FormBuilder,
    private alertCtrl: AlertController,
  ) {}

  ngOnInit() {
    const statusId = [0, 1, 2];
    this.getUser(statusId);
    this.initForm();
  }

  initForm() {
    this.emergencyServiceForm = this.fb.group({
      uidMachine: ['', [Validators.required]],
      date: ['', [Validators.required]],
      hour: ['', [Validators.required]],
      commentary: ['', []],
    })
  }

  new() {
    this.router.navigate(["info-emergencias"])
  }



  viewBitacora(detail) {
    // Data que enviaremos
    let data = detail
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['bitacora-emergencia'], navigationExtras);
  }

  getUser(status) {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getEmergencyServices(user.uid, status);
      }
    )
  }

  getEmergencyServices(uid, status) {
    this.emergencyServices = [];
    this.firestoreService.getEmergencyServicesUser(uid, status)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const emergencyServices = doc.data();
          emergencyServices.uid = doc.id;
          this.emergencyServices.push(emergencyServices);
        });
      });
      console.log(this.emergencyServices)
  }

  disableEmergencyService(uid: string){
    const emergencyService = {
      status: 0
    };

    this.firestoreService.updateEmergencyService(emergencyService, uid).then(() => {
      this.alertMsj(
        "Servicio de Emergencía Cancelado",
        "El servicio de emergencía fue cancelado con exito."
      );
    });
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      mode: 'ios',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }

}
