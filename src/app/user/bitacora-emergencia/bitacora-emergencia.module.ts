import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BitacoraEmergenciaPageRoutingModule } from './bitacora-emergencia-routing.module';

import { BitacoraEmergenciaPage } from './bitacora-emergencia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BitacoraEmergenciaPageRoutingModule
  ],
  declarations: [BitacoraEmergenciaPage]
})
export class BitacoraEmergenciaPageModule {}
