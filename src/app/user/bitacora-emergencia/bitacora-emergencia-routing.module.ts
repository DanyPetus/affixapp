import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BitacoraEmergenciaPage } from './bitacora-emergencia.page';

const routes: Routes = [
  {
    path: '',
    component: BitacoraEmergenciaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BitacoraEmergenciaPageRoutingModule {}
