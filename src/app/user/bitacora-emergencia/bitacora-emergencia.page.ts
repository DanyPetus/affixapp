import {
  Component
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from "@angular/router"
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-bitacora-emergencia',
  templateUrl: './bitacora-emergencia.page.html',
  styleUrls: ['./bitacora-emergencia.page.scss'],
})
export class BitacoraEmergenciaPage {
  data: any;
  machineInfo: any;
  binnacles = [];

  // CAMPOS HTML
  name: any;
  description: any;
  code: any;
  brand: any;
  model: any;
  pathImage: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
    private fb: FormBuilder,
    private alertCtrl: AlertController,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
      this.getMachineInfo(this.data.uidMachine);
    });
  }

  tecnico(bitacora) {
    // Data que enviaremos
    let data = {
      "bitacora": bitacora,
      "machine": this.machineInfo
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['bitacora-maquinas'], navigationExtras);
  }

  async getMachineInfo(uid: string) {
    await this.firestoreService.getMachineByUid(uid).then(machine => {
      this.machineInfo = machine.data();
      this.machineInfo.uid = uid;
      console.log(this.machineInfo)

      this.name = this.machineInfo.name;
      this.description = this.machineInfo.description;
      this.code = this.machineInfo.code;
      this.brand = this.machineInfo.brand;
      this.model = this.machineInfo.model;
      this.pathImage = this.machineInfo.pathImage;

      this.getBinnaclesMachine(this.machineInfo.uid);
    });
  }

  getBinnaclesMachine(uidMachine) {
    this.firestoreService.getBinnaclesMachine(uidMachine)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const binnacles = doc.data();
          binnacles.uid = doc.id;
          this.binnacles.push(binnacles);
        });
        console.log(this.binnacles)
      });
  }

}
