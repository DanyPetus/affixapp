import {
  Component
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.page.html',
  styleUrls: ['./servicios.page.scss'],
})
export class ServiciosPage {
  services = [];

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
    
  ) {}

  ngOnInit() {
    const status = [1];
    this.getUser(status);
  }

  goDetail(detail : any) {
    // Data que enviaremos
    let data = detail
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['bitacora-servicio'], navigationExtras);
  }

  getUser(status) {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getServicesUser(user.uid, status);
      }
    )
  }

  status(ev) {
    let value = ev.detail;
    console.log(value);
    let valor
    if (value.value === "1") {
      valor = [1]
    } else if (value.value === "2") {
      valor = [2]
    } else if (value.value === "1,2,3") {
      valor = [1, 2, 3]
    }
    console.log(valor);

    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getServicesUser(user.uid, valor)
      }
    )
  }

  getServicesUser(uid: any, status) {
    this.services = [];
    this.firestoreService.getServicesUser(uid, status)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const service = doc.data();
          service.uid = doc.id;
          this.services.push(service);
        });
        console.log(this.services)
      });
    // this.servicesFilter = this.services;
  }
}
