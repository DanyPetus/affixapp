import {
  Component
} from '@angular/core';
import {
  Router
} from "@angular/router"
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
// Services
import {
  AuthService
} from 'src/app/services/auth/auth.service';
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  // Variables de inputs
  email: any;
  pass: any;

  userData : any;

  constructor(
    private router: Router,
    private authService: AuthService,
    private firestoreService: FirestoreService,
    private alertCtrl: AlertController,
    private storage: Storage,
    private menu: MenuController,
    public loadingController: LoadingController,
  ) {}

  ngOnInit() {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(false, 'first');
    this.menu.enable(false, 'secondary');
  }

  goHome() {
    this.authService.login(this.email, this.pass)
      .then((user) => {
        this.getValidation(this.email);
      }).catch((error: any) => {
        this.alertMsj(
          "El usuario o la contraseña son incorrectos",
          "Por favor, intente de nuevo..."
        );
      });
  }

  async getValidation(email) {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.storage.set('email', email);
    await this.firestoreService.getUser(email).then(info => {
      info.docs.map(doc => {
        const userData = doc.data();
        if (userData.status === 1) {
          if (userData.login === 0) {
            this.userData = userData;
            this.storage.set('user', this.userData);
            if (userData.roleType === 3) this.router.navigate(["inicio"]);
            else if (userData.roleType === 4) this.router.navigate(["iniciotech"]);
          } else {
            this.storage.set('user', this.userData);
          }
        } else {

        }
      });
    });
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      mode: 'ios',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }


}
