import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CotizacionespdfPageRoutingModule } from './cotizacionespdf-routing.module';

import { CotizacionespdfPage } from './cotizacionespdf.page';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PdfViewerModule,
    CotizacionespdfPageRoutingModule
  ],
  declarations: [CotizacionespdfPage]
})
export class CotizacionespdfPageModule {}
