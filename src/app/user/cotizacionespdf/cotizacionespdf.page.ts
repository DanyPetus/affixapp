import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from "@angular/router"
import {
  DomSanitizer, SafeUrl
} from '@angular/platform-browser';

@Component({
  selector: 'app-cotizacionespdf',
  templateUrl: './cotizacionespdf.page.html',
  styleUrls: ['./cotizacionespdf.page.scss'],
})
export class CotizacionespdfPage implements OnInit {
  pdf: any;
  data : any;

  constructor(
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        let data : any = JSON.parse(params.dataCard);
        let pdf : any = this.sanitizer.bypassSecurityTrustResourceUrl(data);
        this.pdf = pdf.changingThisBreaksApplicationSecurity;
        console.log(this.pdf);
      }
    });
  }

}
