import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CotizacionespdfPage } from './cotizacionespdf.page';

const routes: Routes = [
  {
    path: '',
    component: CotizacionespdfPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CotizacionespdfPageRoutingModule {}
