import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-bitacoras',
  templateUrl: './bitacoras.page.html',
  styleUrls: ['./bitacoras.page.scss'],
})
export class BitacorasPage implements OnInit {
  data : any;
  date : any;

  
  constructor(
    private route: ActivatedRoute, 
    private router: Router,

  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        var time = new Date(this.data.date.seconds*1000 + this.data.date.nanoseconds/100000);
        this.date = time.getDate() + "/" + (time.getMonth() + 1) + "/" + time.getFullYear();
        // console.log(this.date);
      }
    });
  }

}
