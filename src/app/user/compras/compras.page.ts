import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.page.html',
  styleUrls: ['./compras.page.scss'],
})
export class ComprasPage implements OnInit {
  purchaseOrders = [];
  maquinaFiler: string;

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
  ) { }

  ngOnInit() {
    this.getUser();
  }

  searchMaquina() {
    if (this.maquinaFiler != "") {
      this.purchaseOrders = this.purchaseOrders.filter(res => {
        return res.nameMachine.toLocaleLowerCase().match(this.maquinaFiler.toLocaleLowerCase());
      })
    } else if (this.maquinaFiler == "") {
      this.ngOnInit();
    }
}

  getUser() {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getPurchaseOrdersUser(user.uid);
      }
    )
  }

  getPurchaseOrdersUser(uid) {
    this.purchaseOrders = [];
    this.firestoreService.getPurchaseOrdersUser(uid)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const purchaseOrder = doc.data();
          purchaseOrder.uid = doc.id;
          this.purchaseOrders.push(purchaseOrder);
        });
        console.log(this.purchaseOrders)
      });
    // this.purchaseOrdersFilter = this.purchaseOrders;
  }

  viewFile(file:string){
    // window.open(file, "_blank");
        // Data que enviaremos
        let data = file
        let navigationExtras: NavigationExtras = {
          queryParams: {
            // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
            // JSON.stringify(data): Recibe data y la convierte en un JSON
            dataCard: JSON.stringify(data)
          }
        };
        // Ruta de Page, NavigationExtras: Data a enviar
        this.router.navigate(['compraspdf'], navigationExtras);
  }

}
