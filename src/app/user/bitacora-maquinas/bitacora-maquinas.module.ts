import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BitacoraMaquinasPageRoutingModule } from './bitacora-maquinas-routing.module';

import { BitacoraMaquinasPage } from './bitacora-maquinas.page';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PdfViewerModule,
    IonicModule,
    BitacoraMaquinasPageRoutingModule
  ],
  declarations: [BitacoraMaquinasPage]
})
export class BitacoraMaquinasPageModule {}
