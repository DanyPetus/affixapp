import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BitacoraMaquinasPage } from './bitacora-maquinas.page';

const routes: Routes = [
  {
    path: '',
    component: BitacoraMaquinasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BitacoraMaquinasPageRoutingModule {}
