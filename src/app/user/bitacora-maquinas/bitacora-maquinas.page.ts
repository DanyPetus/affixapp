import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-bitacora-maquinas',
  templateUrl: './bitacora-maquinas.page.html',
  styleUrls: ['./bitacora-maquinas.page.scss'],
})
export class BitacoraMaquinasPage implements OnInit {
  data: any;

  bitacora : any;
  maquina : any;
  date : any;
  pdf: any;
  viewFile : boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private firestoreService: FirestoreService,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
      this.bitacora = this.data.bitacora;
      var time = new Date(this.bitacora.date.seconds*1000 + this.bitacora.date.nanoseconds/100000);
      this.date = time.getDate() + "/" + (time.getMonth() + 1) + "/" + time.getFullYear();
      this.maquina = this.data.machine;
      this.pdf = this.bitacora.files[0].path;
      console.log(this.bitacora.files.length)
      if (this.bitacora.files.length >= 1 ){
        this.viewFile = true
      } else {
        this.viewFile = false;
      }
    });
  }

}
