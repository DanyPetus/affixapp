import {
  Component
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-maquinas',
  templateUrl: './maquinas.page.html',
  styleUrls: ['./maquinas.page.scss'],
})
export class MaquinasPage {
  machines = [];
  maquinaFiler: string;
  serieFilter: string;

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,

  ) {}

  ngOnInit() {
    this.getUser();
  }

  searchMaquina() {
    if (this.maquinaFiler != "") {
      this.machines = this.machines.filter(res => {
        return res.name.toLocaleLowerCase().match(this.maquinaFiler.toLocaleLowerCase());
      })
    } else if (this.maquinaFiler == "") {
      this.ngOnInit();
    }
  }

  searchSerie() {
    if (this.serieFilter != "") {
      this.machines = this.machines.filter(res => {
        return res.serialNumber.toLocaleLowerCase().match(this.serieFilter.toLocaleLowerCase());
      })
    } else if (this.serieFilter == "") {
      this.ngOnInit();
    }
  }

  getUser() {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getMachinesAssigned(user.uid);
      }
    )
  }

  getMachinesAssigned(uid) {
    this.machines = [];
    this.firestoreService.getMachinesUser(uid)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const machine = doc.data();
          machine.uid = doc.id;
          this.machines.push(machine);
        });
      });
    console.log(this.machines);
    // this.machinesFilter = this.machines;
  }


  viewFile(pathImage: string) {
    Swal.fire({
      imageUrl: pathImage,
      imageWidth: 200,
      imageHeight: 200,
      imageAlt: 'Custom image',
    })
  }

  machineInfo(machine) {
    // Data que enviaremos
    let data = machine
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['info-maquinas'], navigationExtras);
  }


}
