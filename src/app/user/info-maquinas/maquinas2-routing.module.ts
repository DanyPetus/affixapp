import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Maquinas2Page } from './maquinas2.page';

const routes: Routes = [
  {
    path: '',
    component: Maquinas2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Maquinas2PageRoutingModule {}
