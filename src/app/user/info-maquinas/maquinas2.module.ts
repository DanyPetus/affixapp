import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Maquinas2PageRoutingModule } from './maquinas2-routing.module';

import { Maquinas2Page } from './maquinas2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Maquinas2PageRoutingModule
  ],
  declarations: [Maquinas2Page]
})
export class Maquinas2PageModule {}
