import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompraspdfPageRoutingModule } from './compraspdf-routing.module';

import { CompraspdfPage } from './compraspdf.page';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PdfViewerModule,
    CompraspdfPageRoutingModule
  ],
  declarations: [CompraspdfPage]
})
export class CompraspdfPageModule {}
