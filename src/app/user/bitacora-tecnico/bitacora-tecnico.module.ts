import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BitacoraTecnicoPageRoutingModule } from './bitacora-tecnico-routing.module';

import { BitacoraTecnicoPage } from './bitacora-tecnico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BitacoraTecnicoPageRoutingModule
  ],
  declarations: [BitacoraTecnicoPage]
})
export class BitacoraTecnicoPageModule {}
