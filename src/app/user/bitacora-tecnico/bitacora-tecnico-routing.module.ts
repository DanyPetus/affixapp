import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BitacoraTecnicoPage } from './bitacora-tecnico.page';

const routes: Routes = [
  {
    path: '',
    component: BitacoraTecnicoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BitacoraTecnicoPageRoutingModule {}
