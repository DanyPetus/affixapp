import {
  Component,
} from '@angular/core';
import {
  Router
} from "@angular/router";
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController,
  ModalController
} from '@ionic/angular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage {

  constructor(
    private router: Router,
    private menu: MenuController,
    public loadingController: LoadingController,
  ) {}

  ngOnInit() {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.enable(false, 'secondary');
  }

  async Ir() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["servicios"]);
  }

  async maquinas() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["maquinas"])
  }

  async emergencia() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["emergencias"])
  }

  async compras() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["compras"])
  }

  async cotizar() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["cotizaciones"])
  }
}
