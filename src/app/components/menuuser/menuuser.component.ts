import {
  Component,
  OnInit
} from '@angular/core';
import {
  Storage
} from '@ionic/storage';
import {
  Router,
  NavigationExtras
} from '@angular/router';

@Component({
  selector: 'app-menuuser',
  templateUrl: './menuuser.component.html',
  styleUrls: ['./menuuser.component.scss'],
})
export class MenuuserComponent implements OnInit {

  constructor(
    private storage: Storage,
    public router: Router,
  ) {}

  ngOnInit() {}

  exit() {
    this.storage.clear();
    this.router.navigateByUrl('login');
  }

  regresar(){
    this.router.navigateByUrl('inicio');
  }

}
