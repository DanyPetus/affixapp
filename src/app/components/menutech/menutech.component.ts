import {
  Component,
  OnInit
} from '@angular/core';
import {
  Storage
} from '@ionic/storage';
import {
  Router,
  NavigationExtras
} from '@angular/router';

@Component({
  selector: 'app-menutech',
  templateUrl: './menutech.component.html',
  styleUrls: ['./menutech.component.scss'],
})
export class MenutechComponent implements OnInit {

  constructor(
    private storage: Storage,
    public router: Router,
  ) {}

  ngOnInit() {}

  exit() {
    this.storage.clear();
    this.router.navigateByUrl('login');
  }

  regresar(){
    this.router.navigateByUrl('iniciotech');
  }

}
