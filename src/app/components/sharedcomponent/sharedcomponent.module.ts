import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MenuuserComponent } from '../menuuser/menuuser.component'
import { MenutechComponent } from '../menutech/menutech.component'

@NgModule({
  declarations: [MenuuserComponent, MenutechComponent],
  imports: [ 
    FormsModule,  
    CommonModule,
    IonicModule
  ],
  exports: [MenuuserComponent, MenutechComponent]
})
export class SharedComponentsModule { }