import {
  NgModule
} from '@angular/core';
import {
  BrowserModule
} from '@angular/platform-browser';
import {
  RouteReuseStrategy
} from '@angular/router';

import {
  IonicModule,
  IonicRouteStrategy
} from '@ionic/angular';

import {
  AppComponent
} from './app.component';
import {
  AppRoutingModule
} from './app-routing.module';

// Firebase
import {
  AngularFireModule
} from "@angular/fire";
import {
  AngularFireAuthModule
} from "@angular/fire/auth";
import {
  AngularFireStorageModule,
  BUCKET
} from '@angular/fire/storage';
import {
  AngularFirestoreModule
} from '@angular/fire/firestore';

// Config
import {
  environment
} from 'src/environments/environment';

//plugins
import { IonicStorageModule } from '@ionic/storage';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    SharedComponentsModule,
    ReactiveFormsModule,
    PdfViewerModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
  ],
  providers: [{
    provide: RouteReuseStrategy,
    useClass: IonicRouteStrategy
  }],
  bootstrap: [AppComponent],
})
export class AppModule {}
