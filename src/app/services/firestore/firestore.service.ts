import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  gblUserCollection: AngularFirestoreCollection;
  gblServicesCollection: AngularFirestoreCollection;
  gblMachinesCollection: AngularFirestoreCollection;
  gblEmergencyServicesCollection: AngularFirestoreCollection;
  gblBinnacleCollection: AngularFirestoreCollection;
  gblPurchaseOrdersCollection: AngularFirestoreCollection;
  gblQuotationCollection: AngularFirestoreCollection;
  gblCampusCollection: AngularFirestoreCollection;

  constructor(
    private afStore: AngularFirestore,
  ) { 
    this.gblUserCollection = this.afStore.collection('gbl_user');
    this.gblServicesCollection = this.afStore.collection('gbl_services');
    this.gblMachinesCollection = this.afStore.collection('gbl_machines');
    this.gblEmergencyServicesCollection = this.afStore.collection('gbl_emergency_services');
    this.gblBinnacleCollection = this.afStore.collection('gbl_binnacle');
    this.gblPurchaseOrdersCollection = this.afStore.collection('gbl_purchaseOrder');
    this.gblQuotationCollection = this.afStore.collection('gbl_quotation');
    this.gblCampusCollection = this.afStore.collection('gbl_campus');
  }

  getUser(email: string) {
    const query = this.gblUserCollection.ref.where('email', '==', email);
    return query.get();
  }

  getServicesUser(uid: string, status: any) {
    const query = this.gblServicesCollection.ref.where('uidUser', '==', uid).where('status', 'in', status);
    return query.get();
  }

  getMachinesUser(userAuthUid: string) {
    const query = this.gblMachinesCollection.ref.where('usersAssigned', 'array-contains', userAuthUid);
    return query.get();
  }

  getServices() {
    return this.gblServicesCollection.valueChanges();
  }

  getEmergencyServicesUser(uid: string, status: any) {
    const query = this.gblEmergencyServicesCollection.ref.where('createBy', '==', uid).where('status', 'in', status);
    return query.get();
  }

  getMachineByUid(uid: string) {
    const query = this.gblMachinesCollection.ref.doc(uid);
    return query.get();
  }

  createEmergencyService(emergencyService: any) {
    return this.gblEmergencyServicesCollection.add(emergencyService);
  }

  updateEmergencyService(emergencyService: any, uid: any) {
    return this.gblEmergencyServicesCollection.doc(uid).update(emergencyService);
  }

  getBinnaclesMachine(uid: string) {
    const query = this.gblBinnacleCollection.ref.where('uidMachine', '==', uid);
    return query.get();
  }

  getPurchaseOrdersUser(uid: string) {
    const query = this.gblPurchaseOrdersCollection.ref.where('uidUser', '==', uid);
    return query.get();
  }

  getQuotationsUser(uid: string) {
    const query = this.gblQuotationCollection.ref.where('uidUser', '==', uid);
    return query.get();
  }


  // technical

  getServicesTechnical(uid: string) {
    const query = this.gblServicesCollection.ref.where('uidTechnician', '==', uid).where('status', '==', 1);
    return query.get();
  }

  getUserByUid(uid: string) {
    const query = this.gblUserCollection.ref.where('uid', '==', uid);
    return query.get();
  }

  getCampusByUid(uid: string) {
    const query = this.gblCampusCollection.ref.doc(uid);
    return query.get();
  }

  updateService(service: any, uid: any) {
    return this.gblServicesCollection.doc(uid).update(service);
  }

  createBinnacleCollection(service: any) {
    return this.gblBinnacleCollection.add(service);
  }

  getMachinesCreateBy(uid: string) {
    const query = this.gblMachinesCollection.ref.where('createBy', '==', uid);
    return query.get();
  }
  
  getUsersAll() {
    return this.gblUserCollection.valueChanges();
  }

  getBinnaclesTechnical(uid: string) {
    const query = this.gblBinnacleCollection.ref.where('createBy', '==', uid);
    return query.get();
  }

  getServicesUid(uid: string) {
    const query = this.gblServicesCollection.ref.doc(uid);
    return query.get();
  }

}
