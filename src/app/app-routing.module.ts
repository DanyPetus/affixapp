import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { Maquinas2PageModule } from './user/info-maquinas/maquinas2.module';
import { Maquinas2Page } from './user/info-maquinas/maquinas2.page';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./user/login/login.module').then( m => m.LoginPageModule)
  },


  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  
    {
    path: 'inicio',
    loadChildren: () => import('./user/inicio/inicio.module').then( m => m.InicioPageModule)
  },
  {
    path: 'servicios',
    loadChildren: () => import('./user/servicios/servicios.module').then( m => m.ServiciosPageModule)
  },
  {
    path: 'maquinas',
    loadChildren: () => import('./user/maquinas/maquinas.module').then( m => m.MaquinasPageModule)
  },
  {
    path: 'emergencias',
    loadChildren: () => import('./user/emergencias/emergencias.module').then( m => m.EmergenciasPageModule)
  },
  {
    path: 'compras',
    loadChildren: () => import('./user/compras/compras.module').then( m => m.ComprasPageModule)
  },
  {
    path: 'cotizaciones',
    loadChildren: () => import('./user/cotizaciones/cotizaciones.module').then( m => m.CotizacionesPageModule)
  },
  {
    path: 'info-maquinas',
    loadChildren: () => import('./user/info-maquinas/maquinas2.module').then( m => m.Maquinas2PageModule)
  },
  {
    path: 'info-emergencias',
    loadChildren: () => import('./user/info-emergencias/emergencies.module').then( m => m.EmergenciesPageModule)
  },
  {
    path: 'bitacora-servicio',
    loadChildren: () => import('./user/bitacora-servicio/bitacoras.module').then( m => m.BitacorasPageModule)
  },
  {
    path: 'bitacora-emergencia',
    loadChildren: () => import('./user/bitacora-emergencia/bitacora-emergencia.module').then( m => m.BitacoraEmergenciaPageModule)
  },
  {
    path: 'bitacora-tecnico',
    loadChildren: () => import('./user/bitacora-tecnico/bitacora-tecnico.module').then( m => m.BitacoraTecnicoPageModule)
  },
  {
    path: 'bitacora-maquinas',
    loadChildren: () => import('./user/bitacora-maquinas/bitacora-maquinas.module').then( m => m.BitacoraMaquinasPageModule)
  },
  {
    path: 'iniciotech',
    loadChildren: () => import('./tech/iniciotech/iniciotech.module').then( m => m.IniciotechPageModule)
  },
  {
    path: 'serviciostech',
    loadChildren: () => import('./tech/serviciostech/serviciostech.module').then( m => m.ServiciostechPageModule)
  },
  {
    path: 'bitacoratech',
    loadChildren: () => import('./tech/bitacoratech/bitacoratech.module').then( m => m.BitacoratechPageModule)
  },
  {
    path: 'informaciontech',
    loadChildren: () => import('./tech/informaciontech/informaciontech.module').then( m => m.InformaciontechPageModule)
  },
  {
    path: 'maquinastech',
    loadChildren: () => import('./tech/maquinastech/maquinastech.module').then( m => m.MaquinastechPageModule)
  },
  {
    path: 'infotech',
    loadChildren: () => import('./tech/infotech/infotech.module').then( m => m.InfotechPageModule)
  },
  {
    path: 'infoserviciotech',
    loadChildren: () => import('./tech/infoserviciotech/infoserviciotech.module').then( m => m.InfoserviciotechPageModule)
  },
  {
    path: 'infobitacoratech',
    loadChildren: () => import('./tech/infobitacoratech/infobitacoratech.module').then( m => m.InfobitacoratechPageModule)
  },
  {
    path: 'informacionbitacorastech',
    loadChildren: () => import('./tech/informacionbitacorastech/informacionbitacorastech.module').then( m => m.InformacionbitacorastechPageModule)
  },  {
    path: 'compraspdf',
    loadChildren: () => import('./user/compraspdf/compraspdf.module').then( m => m.CompraspdfPageModule)
  },
  {
    path: 'cotizacionespdf',
    loadChildren: () => import('./user/cotizacionespdf/cotizacionespdf.module').then( m => m.CotizacionespdfPageModule)
  },


  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
