import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-informacionbitacorastech',
  templateUrl: './informacionbitacorastech.page.html',
  styleUrls: ['./informacionbitacorastech.page.scss'],
})
export class InformacionbitacorastechPage implements OnInit {
  data: any;

  binnacleInfo: any;
  serviceInfo: any;
  filesBinnacles: any;
  machineInfo: any;

  // View Validation
  informationCompleted = false;


  // SERVICEINFO
  nameMachine: any;
  nameTechnician: any;
  nameUser: any;
  campus: any;
  building: any;
  hour: any;
  commentary: any;

  // machineInfo
  brand : any;
  serialNumber : any;
  model : any;

  files : any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
        this.files = this.data.files;
      }
      this.getServiceByUid(this.data.uidService);
    });
  }

  async getServiceByUid(uidService: string) {
    await this.firestoreService.getServicesUid(uidService).then(service => {
      this.serviceInfo = service.data();
      this.nameMachine = this.serviceInfo.nameMachine ;
      this.nameTechnician = this.serviceInfo.nameTechnician ;
      this.nameUser = this.serviceInfo.nameUser ;
      this.campus = this.serviceInfo.campus ;
      this.building = this.serviceInfo.building ;
      this.hour = this.serviceInfo.hour ;
      this.commentary = this.serviceInfo.commentary ;
      this.getInformationMachine(this.serviceInfo.uidMachine)
    });
  }

  async getInformationMachine(uidMachine: string) {
    await this.firestoreService.getMachineByUid(uidMachine).then(machine => {
      this.machineInfo = machine.data();
      this.brand = this.machineInfo.brand ;
      this.serialNumber = this.machineInfo.serialNumber ;
      this.model = this.machineInfo.model ;
      this.getUser();
    });
  }

  async getUser() {
    await this.firestoreService.getUserByUid(this.serviceInfo.uidUser).then(user => {
      user.docs.map(doc => {
        const userInfo = doc.data();
        this.serviceInfo.building = userInfo.building;
        this.getCampus(userInfo.campusUid);
      });
    });
  }

  async getCampus(campusUid: string) {
    await this.firestoreService.getCampusByUid(campusUid).then(campus => {
      const campusInfo = campus.data();
      this.serviceInfo.campus = campusInfo.name;
      this.informationCompleted = true;
    });
  }

}
