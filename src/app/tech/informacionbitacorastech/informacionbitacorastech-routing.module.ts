import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformacionbitacorastechPage } from './informacionbitacorastech.page';

const routes: Routes = [
  {
    path: '',
    component: InformacionbitacorastechPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformacionbitacorastechPageRoutingModule {}
