import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformacionbitacorastechPageRoutingModule } from './informacionbitacorastech-routing.module';

import { InformacionbitacorastechPage } from './informacionbitacorastech.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacionbitacorastechPageRoutingModule
  ],
  declarations: [InformacionbitacorastechPage]
})
export class InformacionbitacorastechPageModule {}
