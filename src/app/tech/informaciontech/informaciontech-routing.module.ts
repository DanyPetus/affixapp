import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformaciontechPage } from './informaciontech.page';

const routes: Routes = [
  {
    path: '',
    component: InformaciontechPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformaciontechPageRoutingModule {}
