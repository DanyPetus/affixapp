import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-informaciontech',
  templateUrl: './informaciontech.page.html',
  styleUrls: ['./informaciontech.page.scss'],
})
export class InformaciontechPage implements OnInit {
  data: any;
  client: any;
  statusInfoClient = false;

  // CLIENT DATA
  campusName : any;
  building : any;
  project : any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private firestoreService: FirestoreService,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
        this.getClient(this.data.uidUser);
      }
    });
  }

  async getClient(uidUser) {
    await this.firestoreService.getUserByUid(uidUser).then(info => {
      info.docs.map(doc => {
        this.client = doc.data();
        console.log(this.client);
        this.campusName = this.client.campusName;
        this.building = this.client.building;
        this.project = this.client.project;
        this.getCampus();
      });
    });
  }

  async getCampus() {
    if(this.client.campusUid != '0'){
      await this.firestoreService.getCampusByUid(this.client.campusUid).then(campus => {
        const campusInfo = campus.data();
        this.client.campusName = campusInfo.name
        this.statusInfoClient = true
      });
    }else {
      this.client.campusName = 'Sin asignar';
      this.statusInfoClient = true
    }
  }

}
