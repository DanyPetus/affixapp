import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformaciontechPageRoutingModule } from './informaciontech-routing.module';

import { InformaciontechPage } from './informaciontech.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformaciontechPageRoutingModule
  ],
  declarations: [InformaciontechPage]
})
export class InformaciontechPageModule {}
