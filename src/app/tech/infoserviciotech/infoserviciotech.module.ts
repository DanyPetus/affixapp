import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoserviciotechPageRoutingModule } from './infoserviciotech-routing.module';

import { InfoserviciotechPage } from './infoserviciotech.page';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PdfViewerModule,
    InfoserviciotechPageRoutingModule
  ],
  declarations: [InfoserviciotechPage]
})
export class InfoserviciotechPageModule {}
