import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoserviciotechPage } from './infoserviciotech.page';

const routes: Routes = [
  {
    path: '',
    component: InfoserviciotechPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoserviciotechPageRoutingModule {}
