import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-infoserviciotech',
  templateUrl: './infoserviciotech.page.html',
  styleUrls: ['./infoserviciotech.page.scss'],
})
export class InfoserviciotechPage implements OnInit {
  data : any;
  date : any;
  pdf: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
        var time = new Date(this.data.service.date.seconds*1000 + this.data.service.date.nanoseconds/100000);
        this.date = time.getDate() + "/" + (time.getMonth() + 1) + "/" + time.getFullYear();
      }
      this.pdf = this.data.service.files[0].path;
    });
  }

}
