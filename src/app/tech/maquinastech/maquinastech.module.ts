import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaquinastechPageRoutingModule } from './maquinastech-routing.module';

import { MaquinastechPage } from './maquinastech.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaquinastechPageRoutingModule
  ],
  declarations: [MaquinastechPage]
})
export class MaquinastechPageModule {}
