import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-maquinastech',
  templateUrl: './maquinastech.page.html',
  styleUrls: ['./maquinastech.page.scss'],
})
export class MaquinastechPage implements OnInit {
  // Machines
  machines = [];

  // Users
  user: any;
  users = [];
  arrAssigned = [];
  userAssignedInfo: any;

  maquinaFiler: string;
  serieFilter : string;
  
  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
  ) {}

  ngOnInit() {
    this.getUser();
    this.getUsers();
  }

  searchMaquina() {
    if (this.maquinaFiler != "") {
      this.machines = this.machines.filter(res => {
        return res.name.toLocaleLowerCase().match(this.maquinaFiler.toLocaleLowerCase());
      })
    } else if (this.maquinaFiler == "") {
      this.ngOnInit();
    }
  }

  searchSerie() {
    if (this.serieFilter != "") {
      this.machines = this.machines.filter(res => {
        return res.serialNumber.toLocaleLowerCase().match(this.serieFilter.toLocaleLowerCase());
      })
    } else if (this.serieFilter == "") {
      this.ngOnInit();
    }
  }

  async getUsers() {
    this.firestoreService.getUsersAll().subscribe((users) => {
      this.users = users;
    });
  }

  async saveUserSelect(event: any) {
    const uidUser = event.target.value;
    await this.firestoreService.getUserByUid(uidUser).then(info => {
      info.docs.map(doc => {
        this.userAssignedInfo = doc.data();
        console.log(this.userAssignedInfo)
      });
      this.getMachinesCreateByUser(this.userAssignedInfo);
    });
  }

  getMachinesCreateByUser(user: any) {
    this.arrAssigned = [];
    this.machines = [];
    this.firestoreService.getMachinesCreateBy(this.user.createBy)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const machine = doc.data();
          machine.uid = doc.id;
          this.arrAssigned.push(machine);
        });
        this.arrAssigned.forEach(element => {
          if (element.usersAssigned[1] === user.uid) {
            this.machines.push(element);
          } else {
          }
        });
        console.log(this.machines)
      });
  }

  getUser() {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getUserInfo(user.uid);
      }
    )
  }

  async getUserInfo(uid) {
    await this.firestoreService.getUserByUid(uid).then(info => {
      info.docs.map(doc => {
        this.user = doc.data();
        this.getMachines();
      });
    });
  }

  getMachines() {
    this.machines = [];
    this.firestoreService.getMachinesCreateBy(this.user.createBy)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const machine = doc.data();
          machine.uid = doc.id;
          this.machines.push(machine);
        });
      });
    // this.machinesFilter = this.machines;
  }

  viewFile(pathImage: string) {
    Swal.fire({
      imageUrl: pathImage,
      imageWidth: 400,
      imageHeight: 400,
      imageAlt: 'Custom image',
    })
  }

  infomaquina(machine) {
    // Data que enviaremos
    let data = machine
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['infotech'], navigationExtras);
  }

}
