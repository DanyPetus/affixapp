import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IniciotechPageRoutingModule } from './iniciotech-routing.module';

import { IniciotechPage } from './iniciotech.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IniciotechPageRoutingModule
  ],
  declarations: [IniciotechPage]
})
export class IniciotechPageModule {}
