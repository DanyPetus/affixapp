import {
  Component
} from '@angular/core';
import {
  Router
} from "@angular/router";
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-iniciotech',
  templateUrl: './iniciotech.page.html',
  styleUrls: ['./iniciotech.page.scss'],
})
export class IniciotechPage {

  constructor(
    private router: Router,
    private menu: MenuController,
    public loadingController: LoadingController,
    ) {}

  ngOnInit() {
    this.openFirst();
  }


  openFirst() {
    this.menu.enable(false, 'first');
    this.menu.enable(true, 'secondary');
  }

  async serviciostech() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["serviciostech"])
  }

  async maquina() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["maquinastech"])
  }

  async bitacora() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 2000,
      mode: 'ios'
    });
    await loading.present();
    this.router.navigate(["infobitacoratech"])
  }

}
