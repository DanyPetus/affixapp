import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IniciotechPage } from './iniciotech.page';

const routes: Routes = [
  {
    path: '',
    component: IniciotechPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IniciotechPageRoutingModule {}
