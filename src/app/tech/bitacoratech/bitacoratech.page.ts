import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-bitacoratech',
  templateUrl: './bitacoratech.page.html',
  styleUrls: ['./bitacoratech.page.scss'],
})
export class BitacoratechPage implements OnInit {
  data: any;

  // Path Files
  files = [];
  pathFile0: any;
  pathFile1: any;
  pathFile2: any;
  pathFile3: any;
  pathFile4: any;

  commentary : any;
  addCommentary : any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private firestoreService: FirestoreService,
    private storageFire: AngularFireStorage,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
    });
  }

  fileBinnacle(numberfile: number, event: any) {
    if (numberfile === 0) this.pathFile0 = event.target.files[0];
    if (numberfile === 1) this.pathFile1 = event.target.files[0];
    if (numberfile === 2) this.pathFile2 = event.target.files[0];
    if (numberfile === 3) this.pathFile3 = event.target.files[0];
    if (numberfile === 4) this.pathFile4 = event.target.files[0];
  }

  async createBinnacle(){
    await this.uploadFileBinnacle();
    this.getUser();
  }

  async uploadFileBinnacle() {
    for (let index = 0; index < 5; index++) {
      const file = this[`pathFile${index}`];
      if (file) {
        const path = await this.createFileMachine(file);
        const namesArray = file.name.split('.');
        const type = namesArray[namesArray.length - 1]
        let typeValue = 'img';
        if (type === 'pdf') typeValue = 'pdf';

        this.files.push({
          path,
          type: typeValue
        });
      }
    }
  }

  async createFileMachine(file: any) {
    try {
      const code = Math.random().toString(36).substring(7);
      const name = Math.random().toString(36).substring(7) + code;
      await this.storageFire.ref(`/binnacle/${this.data.nameMachine}`).child(name).put(file);
      return this.storageFire.ref(`binnacle/${this.data.nameMachine}/${name}`).getDownloadURL().toPromise();
    } catch (error) {
      console.log(error);
    }
  }

  getUser() {
    this.storage.get("user").then(
      (user) => {
        // console.log(user);
        this.getServicesUser(user.uid);
      }
    )
  }

  getServicesUser(uid) {
    const binnacleInfo = {
      uidService: this.data.uid,
      uidMachine: this.data.uidMachine,
      nameMachine: this.data.nameMachine,
      nameTechnician: this.data.nameTechnician,
      uidTechnician: this.data.uidTechnician,
      commentary: this.commentary,
      files: this.files,
      status: 1,
      createBy: uid,
      createDate: new Date(),
      updateBy: uid,
      updateDate: new Date()
    };
    // console.log(binnacleInfo);

    this.firestoreService.createBinnacleCollection(binnacleInfo).then(() => {
      this.handleUpdateService(uid);
    });
  }

  handleUpdateService(uid) {
    const serviceInfo = {
      binnacle: 1,
      status: 2,
      updateBy: uid,
      updateDate: new Date()
    };

    this.firestoreService.updateService(serviceInfo, this.data.uid).then(() => {
      this.alertMsj(
        "Bítacora Guardada",
        "La Bítacora fue guardada con exito."
      );
      this.router.navigate(["serviciostech"]);
    });
  }


  createComment() {
    const serviceInfo = {
      mensaje: this.addCommentary
    };
    // console.log(serviceInfo);

    this.firestoreService.updateService(serviceInfo, this.data.uid).then(() => {
      this.alertMsj(
        "Bítacora Guardada",
        "La Bítacora fue guardada con exito."
      );
      this.router.navigate(["serviciostech"]);
    });
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      mode: 'ios',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }

}
