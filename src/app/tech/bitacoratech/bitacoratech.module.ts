import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BitacoratechPageRoutingModule } from './bitacoratech-routing.module';

import { BitacoratechPage } from './bitacoratech.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BitacoratechPageRoutingModule
  ],
  declarations: [BitacoratechPage]
})
export class BitacoratechPageModule {}
