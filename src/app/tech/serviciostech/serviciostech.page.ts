import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-serviciostech',
  templateUrl: './serviciostech.page.html',
  styleUrls: ['./serviciostech.page.scss'],
})
export class ServiciostechPage implements OnInit {
  // Services
  services = [];

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
  ) {}

  ngOnInit() {
    this.getUser();
  }

  serviceBinnacle(service) {
    // Data que enviaremos
    let data = service
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['bitacoratech'], navigationExtras);
  }

  serviceInfo(service) {
    // Data que enviaremos
    let data = service
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['informaciontech'], navigationExtras);
  }

  getUser() {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getServicesTechnical(user.uid);
      }
    )
  }

  getServicesTechnical(uid) {
    this.services = [];
    this.firestoreService.getServicesTechnical(uid)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const service = doc.data();
          service.uid = doc.id;
          this.services.push(service);
        });
        console.log(this.services)
      });
    // this.servicesFilter = this.services;
  }

}
