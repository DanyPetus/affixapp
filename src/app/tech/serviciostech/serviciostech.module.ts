import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServiciostechPageRoutingModule } from './serviciostech-routing.module';

import { ServiciostechPage } from './serviciostech.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ServiciostechPageRoutingModule
  ],
  declarations: [ServiciostechPage]
})
export class ServiciostechPageModule {}
