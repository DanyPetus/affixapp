import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServiciostechPage } from './serviciostech.page';

const routes: Routes = [
  {
    path: '',
    component: ServiciostechPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiciostechPageRoutingModule {}
