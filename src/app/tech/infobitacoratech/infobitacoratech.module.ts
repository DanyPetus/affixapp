import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfobitacoratechPageRoutingModule } from './infobitacoratech-routing.module';

import { InfobitacoratechPage } from './infobitacoratech.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfobitacoratechPageRoutingModule
  ],
  declarations: [InfobitacoratechPage]
})
export class InfobitacoratechPageModule {}
