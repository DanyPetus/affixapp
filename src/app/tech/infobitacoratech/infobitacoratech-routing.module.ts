import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfobitacoratechPage } from './infobitacoratech.page';

const routes: Routes = [
  {
    path: '',
    component: InfobitacoratechPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfobitacoratechPageRoutingModule {}
