import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from "@angular/router"
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-infobitacoratech',
  templateUrl: './infobitacoratech.page.html',
  styleUrls: ['./infobitacoratech.page.scss'],
})
export class InfobitacoratechPage implements OnInit {
  binnacles = [];

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    private storage: Storage,
  ) {}

  ngOnInit() {
    this.getUser();
  }

  binnacleInfo(bitacora) {
    // Data que enviaremos
    let data = bitacora
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['informacionbitacorastech'], navigationExtras);
  }

  getUser() {
    this.storage.get("user").then(
      (user) => {
        console.log(user);
        this.getBinnaclesTechnical(user.uid);
      }
    )
  }

  getBinnaclesTechnical(uid) {
    this.binnacles = [];
    this.firestoreService.getBinnaclesTechnical(uid)
      .then((query: QuerySnapshot < any > ) => {
        query.docs.forEach((doc: QueryDocumentSnapshot < any > ) => {
          const binnacles = doc.data();
          binnacles.uid = doc.id;
          this.binnacles.push(binnacles);
        });
        console.log(this.binnacles);
      });
    // this.binnaclesFilter = this.binnacles;
  }


}
