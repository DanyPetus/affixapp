import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
//plugins
import {
  Storage
} from '@ionic/storage';
import {
  QuerySnapshot,
  QueryDocumentSnapshot
} from '@angular/fire/firestore';

@Component({
  selector: 'app-infotech',
  templateUrl: './infotech.page.html',
  styleUrls: ['./infotech.page.scss'],
})
export class InfotechPage implements OnInit {
  data: any;
  arrAssigned = [];
  services: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private firestoreService: FirestoreService,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
      this.getServices();
    });
  }

  async getServices() {
    this.firestoreService.getServices().subscribe((services) => {
      this.services = services;
      // console.log(services);
      this.services.forEach(element => {
        if (element.uidMachine === this.data.uid) {
          this.arrAssigned.push(element);
        } else {

        }
      });
      console.log(this.arrAssigned)

    });
  }

  binnacleInfo(service) {
    // Data que enviaremos
    let data = {
      "service": service,
      "machine": this.data
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['infoserviciotech'], navigationExtras);
  }
}
